# Crud Laravel

1. Instalar [Composer](https://getcomposer.org/)
2. Ejecutar `composer install` para instalar dependencias
3. Crear una base de datos local
4. Modificar archivo env.example para incluir el nombre de tu base de datos (algo como DB_DATABASE=crudlaravel)
5. Ejecutar `php artisan serve`, y probablemente te pida generar un certificado y hacer las migraciones

Es posible tener un error por el cotejamiento de la base de datos, es necesario modificar los cotejamientos de tu BD o modificar los valores en config/database.php.
