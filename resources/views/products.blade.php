<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Crud Productos</title>

  <!-- Styles -->
  <link href={{ url('css/bootstrap.min.css') }} rel="stylesheet">
  <link href={{ url('css/datatables.min.css') }} rel="stylesheet">

  <!-- Scripts -->
  <script src={{ url('js/jquery-3.6.0.min.js') }}></script>
  <script src={{ url('js/popper.min.js') }}></script>
  <script src={{ url('js/bootstrap.min.js') }}></script>
  <script src={{ url('js/datatables.min.js') }}></script>
</head>
<body>
  <div class="container mt-5">
    <div class="row">
      <div class="col-12">
        <table class="table table-striped table-bordered table-hover" id="tablaarticulos">
          <thead>
            <tr style="font-weight: bold;">
              <td>Código</td>
              <td>Descripción</td>
              <td>Precio</td>
              <td>Modificar</td>
              <td>Borrar</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($products as $product)
            <tr>
              <th>{{ $product->id }}</th>
              <td>{{ $product->descripcion }}</td>
              <td>{{ $product->precio }}</td>
              <td>
                <a href="{{ route('products.edit', $product->id) }}">
                  <button class="btn btn-warning">Editar</button>
                </a>
              </td>
              <td>
                  <form action="{{ route('products.destroy', $product->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                  </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <a href="{{ route('products.create') }}">
          <button class="btn btn-sm btn-primary" id="BotonAgregar">Agregar artículo</button>
        </a>
      </div>
    </div>
  
    <!-- Formulario (Agregar, Modificar) -->
    <!-- <div class="modal fade" id="FormularioArticulo" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" id="Codigo">
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>Descripción:</label>
                <input type="text" id="Descripcion" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>Precio:</label>
                <input type="number" id="Precio" class="form-control" placeholder="">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" id="ConfirmarAgregar" class="btn btn-success">Agregar</button>
              <button type="button" id="ConfirmarModificar" class="btn btn-info">Modificar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div> -->
  </div>
</body>
</html>
