<form method="post" action="{{ route('products.update', $product->id) }}">  
   @csrf
   @method('PATCH')     
          <div class="form-group">      
              <label for="descripcion">Descripcion:</label><br/><br/>  
              <input type="text" class="form-control" name="descripcion" value="{{ $product->descripcion }}"/><br/><br/>  
          </div>  
<div class="form-group">      
<label for="precio">Precio:</label><br/><br/>  
              <input type="number" step=0.01 class="form-control" name="precio" value="{{ $product->precio }}"/><br/><br/>  
          </div>  
<br/>  
<button type="submit" class="btn-btn">Modificar</button>
</form>  